package com.hendisantika.springbootdockerdemo.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-docker-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/12/19
 * Time: 14.34
 */
public class InvalidTransactionReferenceException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public InvalidTransactionReferenceException(String errorMessage) {
        super(errorMessage);
    }

}