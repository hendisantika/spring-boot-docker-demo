package com.hendisantika.springbootdockerdemo.repository;

import com.hendisantika.springbootdockerdemo.model.Transaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-docker-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/12/19
 * Time: 14.33
 */
@Repository
public interface TransactionRepository extends CrudRepository<Transaction, Long> {

}